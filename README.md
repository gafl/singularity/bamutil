# bamutil Singularity container
### Bionformatics package bamutil<br>
Programs that perform operations on SAM/BAM files, all built into a single executable, bam.<br>
bamutil Version: 1.0.14<br>
[https://github.com/statgen/bamUtil]

Singularity container based on the recipe: Singularity.bamutil_v1.0.14

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build bamutil_v1.0.14.sif Singularity.bamutil_v1.0.14`

### Get image help
`singularity run-help ./bamutil_v1.0.14.sif`

#### Default runscript: STAR
#### Usage:
  `bamutil_v1.0.14.sif --help`<br>
    or:<br>
  `singularity exec bamutil_v1.0.14.sif bam --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull bamutil_v1.0.14.sif oras://registry.forgemia.inra.fr/gafl/singularity/bamutil/bamutil:latest`


